# Changelog `GWIiv`

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [Unreleased]

## [0.1.1] - 2022-09-02
### Fixed
* `Watcher.downloadFile` now uses configuration value `gwiiv.download.max-csv-line-length`

## [0.1.0] - 2022-09-02
### Added
* Initialized Project
* Init git database and add remote
* Create TaskManager and Download watchers