FROM openjdk:11.0-jre


COPY app/build/libs/app-0.1.0-SNAPSHOT-shaded.jar /app/

EXPOSE 8080

CMD ["java", "-jar", "/app/app-0.1.0-SNAPSHOT-shaded.jar"]