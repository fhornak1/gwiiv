package gwiiv.app.conf

import cats.implicits._
import com.typesafe.config.Config
import gwiiv.config.ConfigParser
import gwiiv.config.ConfigParser.{ ValidatedConf, checked }

import java.time.Duration

case class DownloadConf(
  dir: String,
  onCancel: CanceledOrFailed,
  maxCsvLineLength: Int,
  askTimeout: Duration)

object DownloadConf extends ConfigParser[DownloadConf] {
  override val Path: String = "gwiiv.download"

  private val DirPath           = varPath("dir")
  private val MaxCsvLineLenPath = varPath("max-csv-line-length")
  private val AskTimeoutPath    = varPath("ask-timeout")

  override def parse(conf: Config): ValidatedConf[DownloadConf] =
    (
      checked(conf.getString(DirPath)),
      CanceledOrFailed.parse(conf),
      checked(conf.getInt(MaxCsvLineLenPath)),
      checked(conf.getDuration(AskTimeoutPath))).mapN(DownloadConf.apply)
}
