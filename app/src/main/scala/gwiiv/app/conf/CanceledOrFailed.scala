package gwiiv.app.conf

import cats.implicits._
import com.typesafe.config.{ Config, ConfigException }
import gwiiv.config.ConfigParser
import gwiiv.config.ConfigParser.{ ValidatedConf, checked }

sealed trait CanceledOrFailed

object CanceledOrFailed extends ConfigParser[CanceledOrFailed] {
  final case object Leave  extends CanceledOrFailed
  final case object Delete extends CanceledOrFailed

  override val Path: String = DownloadConf.Path

  private val OnCancel = varPath("on-cancel")

  override def parse(conf: Config): ValidatedConf[CanceledOrFailed] =
    checked(conf.getString(OnCancel)).andThen {
      case "leave" =>
        Leave.valid
      case "delete" =>
        Delete.valid
      case op =>
        new ConfigException.BadValue(
          OnCancel,
          s"Unknown on cancel operation: '$op', valid options are: ['leave', 'delete']")
          .invalidNec
    }
}
