package gwiiv.app

import gwiiv.app.http.TaskStatus

object Implicits {

  implicit class TaskStateOps(val ts: TaskState) extends AnyVal {

    def toHttpResponse: Option[TaskStatus] =
      ts match {
        case TaskState.Scheduled(_, taskId) =>
          Some(TaskStatus(taskId, 0, 0.0, "SCHEDULED"))
        case TaskState.Running(_, taskId, linesRead, avgLinesPerSecond, _) =>
          Some(TaskStatus(taskId, linesRead, avgLinesPerSecond, "RUNNING"))
        case TaskState.Done(_, taskId, linesRead, avgLinesPerSec, _) =>
          Some(TaskStatus(taskId, linesRead, avgLinesPerSec, "DONE"))
        case TaskState.Failed(_, taskId) =>
          Some(TaskStatus(taskId, 0, 0.0, "FAILED"))
        case TaskState.Canceled(_, taskId) =>
          Some(TaskStatus(taskId, 0, 0.0, "CANCELED"))
        case TaskState.Unknown =>
          None
      }

    def isTerminal: Boolean =
      ts match {
        case _: TaskState.Scheduled | _: TaskState.Running =>
          false
        case _: TaskState.Done | _: TaskState.Failed | _: TaskState.Canceled |
            TaskState.Unknown =>
          true
      }
  }
}
