package gwiiv.app

object TaskState {

  final case class Scheduled(
    uri: String,
    taskId: Int)
      extends TaskState

  final case class Running(
    uri: String,
    taskId: Int,
    linesRead: Int,
    avgLinesPerSecond: Double,
    resultUri: String)
      extends TaskState

  final case class Done(
    uri: String,
    taskId: Int,
    linesRead: Int,
    avgLinesPerSecond: Double,
    resultUri: String)
      extends TaskState

  final case class Failed(
    uri: String,
    taskId: Int)
      extends TaskState

  final case class Canceled(
    uri: String,
    taskId: Int)
      extends TaskState

  final case object Unknown extends TaskState
}

sealed trait TaskState
