package gwiiv.app.http

import akka.http.scaladsl.common.{
  EntityStreamingSupport,
  JsonEntityStreamingSupport
}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{ DefaultJsonProtocol, RootJsonFormat }

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val newTask: RootJsonFormat[NewTask]       = jsonFormat2(NewTask)
  implicit val taskStatus: RootJsonFormat[TaskStatus] = jsonFormat4(TaskStatus)

  implicit val jsonStreamingSupport: JsonEntityStreamingSupport =
    EntityStreamingSupport.json()
}
