package gwiiv.app

package object http {
  case class NewTask(uri: String, taskId: Int)
  case class TaskStatus(id: Int, lines: Int, avgLinesPerSecond: Double, state: String)
}
