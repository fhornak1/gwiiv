package gwiiv.app.http

import akka.actor.typed.scaladsl.AskPattern._
import akka.{ Done, NotUsed }
import akka.actor.typed.{ ActorRef, ActorSystem }
import akka.http.scaladsl.Http
import akka.http.scaladsl.common.EntityStreamingSupport
import akka.http.scaladsl.common.JsonEntityStreamingSupport
import akka.http.scaladsl.model.{ HttpEntity, MediaTypes, StatusCodes }
import akka.http.scaladsl.server.{ Directives, Route }
import akka.stream.scaladsl.Source
import akka.util.Timeout
import gwiiv.app.Implicits._
import gwiiv.app.TaskState
import gwiiv.app.conf.DownloadConf
import gwiiv.app.convertor.TaskManager

import java.io.File
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.util.{ Failure, Success }

class HttpServer(
  manager: ActorRef[TaskManager.Command],
  conf: DownloadConf
)(
  implicit
  system: ActorSystem[_])
    extends Directives
    with JsonSupport {
  implicit val timeout: Timeout = Timeout.create(conf.askTimeout)
  import system.executionContext

  private def getTasks: Future[List[Int]] = manager.ask(TaskManager.ListTasks)

  private def scheduleConversion(uri: String): Future[Int] =
    manager.ask(ref => TaskManager.NewTask(uri, ref))

  private def getTaskStatus(
    taskId: Int
  ): Future[Either[Option[TaskStatus], Source[TaskStatus, NotUsed]]] =
    manager
      .ask[TaskState](ref => TaskManager.GetTaskStatus(taskId, ref))
      .map { ts =>
        if (ts.isTerminal)
          Left(ts.toHttpResponse)
        else {
          Right(
            Source
              .single(ts)
              .concat(
                Source
                  .tick(2.seconds, 2.seconds, 1)
                  .mapAsync(1)(_ => getTaskState(taskId)))
              .takeWhile(!_.isTerminal)
              .map(_.toHttpResponse.get))
        }
      }

  private def getTaskState(taskId: Int): Future[TaskState] =
    manager.ask(ref => TaskManager.GetTaskStatus(taskId, ref))

  private def cancelTask(taskId: Int): Future[Done] =
    manager.ask(ref => TaskManager.Cancel(taskId, ref))

  private def getFileName(taskId: Int): Future[Option[String]] =
    manager.ask(ref => TaskManager.ReadFile(taskId, ref))

  def start(): Unit = startHttpServer(routes)

  private def startHttpServer(
    routes: Route
  ): Unit = {
    val futureBinding = Http().newServerAt("localhost", 8080).bind(routes)
    futureBinding.onComplete {
      case Success(binding) =>
        val address = binding.localAddress
        system
          .log
          .info(
            "Server online at http://{}:{}/",
            address.getHostString,
            address.getPort)
      case Failure(ex) =>
        system.log.error("Failed to bind HTTP endpoint, terminating system", ex)
        system.terminate()
    }
  }

  implicit val jsonStreamSupport: JsonEntityStreamingSupport =
    EntityStreamingSupport.json()

  lazy val routes: Route = concat(
    path("task") {
      get {
        complete(getTasks)
      }
    },
    path("task") {
      post {
        entity(as[String]) { uri =>
          onComplete(scheduleConversion(uri)) {
            case Failure(exception) =>
              failWith(exception)
            case Success(taskId) =>
              complete(StatusCodes.OK, NewTask(uri, taskId))
          }
        }
      }
    },
    path("task" / IntNumber) { id =>
      concat(
        get {
          onComplete(getTaskStatus(id)) {
            case Failure(exception) =>
              failWith(exception)
            case Success(Left(Some(completed))) =>
              complete(StatusCodes.OK, completed)
            case Success(Left(None)) =>
              complete(StatusCodes.NotFound)
            case Success(Right(stream)) =>
              complete(stream)
          }
        },
        delete {
          complete(cancelTask(id))
        }
      )
    },
    path("download" / IntNumber) { id =>
      get {
        onComplete(getFileName(id)) {
          case Failure(exception) =>
            failWith(exception)
          case Success(Some(filename)) =>
            complete(
              HttpEntity.fromFile(
                MediaTypes.`application/json`,
                new File(conf.dir, filename)))
          case Success(None) =>
            complete(StatusCodes.NotFound)
        }
      }
    }
  )
}
