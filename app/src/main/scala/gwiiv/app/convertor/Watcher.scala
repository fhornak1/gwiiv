package gwiiv.app.convertor

import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }
import akka.actor.typed.{ ActorRef, ActorSystem, Behavior }
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.stream.alpakka.csv.scaladsl.{ CsvParsing, CsvToMap }
import akka.stream.scaladsl.{ FileIO, Keep, Sink }
import akka.stream.{ IOResult, KillSwitches, UniqueKillSwitch }
import akka.util.ByteString
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import gwiiv.app.TaskState
import gwiiv.app.conf.DownloadConf

import java.nio.file.{ Path, Paths }
import java.util.UUID
import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{ Failure, Success }

object Watcher {

  sealed trait Command

  final case class GetDetail(replyTo: ActorRef[TaskState]) extends Command
  final case object Cancel                                 extends Command

  final private case class DownloadStarted(
    ks: UniqueKillSwitch,
    done: Future[IOResult])
      extends Command

  final private case object TaskDone extends Command

  final private case object TaskFailed extends Command

  final private case object IncLines extends Command

  def apply(
    taskId: Int,
    uri: String,
    ref: ActorRef[TaskManager.Command],
    conf: DownloadConf
  ): Behavior[Command] =
    Behaviors
      .setup(ctx => new Watcher(ctx, conf, taskId, uri, ref).startDownload)

  // don't have time to change it to spray
  private lazy val mapper = JsonMapper
    .builder()
    .addModule(DefaultScalaModule)
    .build()

  private def writeJson(obj: Map[String, String]): ByteString =
    ByteString.fromString(mapper.writeValueAsString(obj) + "\n")
}

class Watcher(
  ctx: ActorContext[Watcher.Command],
  conf: DownloadConf,
  taskId: Int,
  uri: String,
  ref: ActorRef[TaskManager.Command],
  startTime: Long = System.currentTimeMillis(),
  filename: String = UUID.randomUUID().toString + ".json") {
  import Watcher._

  def startDownload: Behavior[Command] =
    Behaviors.withStash(3) { buffer =>
      ctx.pipeToSelf(
        downloadFile(uri, Paths.get(conf.dir, filename), ctx.self)(
          ctx.system,
          ctx.executionContext)) {
        case Failure(ex) =>
          ctx.log.error("Download failed", ex)
          TaskFailed
        case Success((killSwitch, result)) =>
          ctx.log.info("Download started")
          DownloadStarted(killSwitch, result)
      }
      Behaviors.receiveMessagePartial {
        case DownloadStarted(ks, done) =>
          buffer.unstashAll(behavior(0, ks, done))
        case TaskFailed =>
          ref ! TaskManager.TaskFailed(taskId, uri)
          Behaviors.stopped
        case m: GetDetail =>
          buffer.stash(m)
          Behaviors.same
        case Cancel =>
          buffer.stash(Cancel)
          Behaviors.same
      }
    }

  def behavior(
    linesRead: Int,
    killSwitch: UniqueKillSwitch,
    done: Future[IOResult]
  ): Behavior[Command] = {
    ctx.pipeToSelf(done) {
      case Failure(ex) =>
        ctx.log.error("Download task failed", ex)
        TaskFailed
      case Success(_) =>
        ctx.log.info("Download task {} finished successfully", taskId)
        TaskDone
    }
    Behaviors.receiveMessagePartial {
      case GetDetail(replyTo) =>
        ctx.log.debug("Getting detail for task: {}", taskId)
        replyTo !
          TaskState.Running(
            uri,
            taskId,
            linesRead,
            linesRead.toDouble / secondsSinceStart,
            filename)
        Behaviors.same
      case TaskDone =>
        ctx.log.info("Task {}: DONE", taskId)
        ref !
          TaskManager.TaskDone(
            taskId,
            uri,
            filename,
            linesRead,
            linesRead.toDouble / secondsSinceStart)
        Behaviors.stopped
      case TaskFailed =>
        ctx.log.error("Task {}: FAILED", taskId)
        ref ! TaskManager.TaskFailed(taskId, uri)
        Behaviors.stopped
      case IncLines =>
        behavior(linesRead + 1, killSwitch, done)
      case Cancel =>
        ctx.log.info("Task {}: CANCELED", taskId)
        killSwitch.shutdown()
        ref ! TaskManager.TaskCanceled(taskId, uri)
        Behaviors.stopped
    }
  }

  private def downloadFile(
    uri: String,
    output: Path,
    ref: ActorRef[Command]
  )(
    implicit
    as: ActorSystem[_],
    ec: ExecutionContext
  ): Future[(UniqueKillSwitch, Future[IOResult])] =
    Http()
      .singleRequest(RequestBuilding.Get(uri))
      .map(
        _.entity
          .dataBytes
          .viaMat(KillSwitches.single)(Keep.right)
          .via(
            CsvParsing.lineScanner(maximumLineLength = conf.maxCsvLineLength))
          .wireTap(_ => ref ! IncLines)
          .via(CsvToMap.toMapAsStrings())
          .map(writeJson)
          .toMat(FileIO.toPath(output))(Keep.both)
          .run())

  private def secondsSinceStart: Long =
    (System.currentTimeMillis() - startTime) / 1000
}
