package gwiiv.app.convertor

import akka.Done
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import cats.data.NonEmptyList
import gwiiv.app.TaskState
import gwiiv.app.conf.DownloadConf

import java.io.File
import scala.collection.immutable

object TaskManager {

  sealed trait Command

  final case class NewTask(
    uri: String,
    replyTo: ActorRef[Int])
      extends Command
  final case class ListTasks(replyTo: ActorRef[List[Int]]) extends Command

  final case class GetTaskStatus(
    taskId: Int,
    replyTo: ActorRef[TaskState])
      extends Command

  final case class Cancel(
    taskId: Int,
    replyTo: ActorRef[Done])
      extends Command

  final case class ReadFile(
    taskId: Int,
    replyTo: ActorRef[Option[String]])
      extends Command
  final case object ShutDown extends Command

  final private[convertor] case class TaskDone(
    taskId: Int,
    uri: String,
    output: String,
    lines: Int,
    avgLinesPerSec: Double)
      extends Command

  final private[convertor] case class TaskFailed(
    taskId: Int,
    uri: String)
      extends Command

  final private[convertor] case class TaskCanceled(
    taskId: Int,
    uri: String)
      extends Command

  def apply(conf: DownloadConf): Behavior[Command] =
    Behaviors.setup(ctx => new TaskManager(ctx, conf).init)
}

class TaskManager(
  ctx: ActorContext[TaskManager.Command],
  conf: DownloadConf) {
  import TaskManager._

  def init: Behavior[Command] = {
    val dirFile = new File(conf.dir)
    if (!dirFile.exists()) {
      dirFile.mkdirs()
    }
    Behaviors.receiveMessagePartial {
      case nt @ NewTask(_, replyTo) =>
        val work = spawnWatcher(ctx, nt.toScheduled(1))
        replyTo ! 1
        empty(2, Map(1 -> work), Map())
      case ListTasks(replyTo) =>
        replyTo ! List()
        Behaviors.same
      case GetTaskStatus(_, replyTo) =>
        replyTo ! TaskState.Unknown
        Behaviors.same
      case ShutDown =>
        Behaviors.stopped
    }
  }

  // TaskManager does not have any scheduled tasks
  def empty(
    id: Int,
    workers: Map[Int, ActorRef[Watcher.Command]],
    completed: Map[Int, TaskState]
  ): Behavior[Command] =
    Behaviors.receiveMessage {
      case nt @ NewTask(_, replyTo) if workers.size < 2 =>
        val worker = spawnWatcher(ctx, nt.toScheduled(id))
        replyTo ! id
        empty(id + 1, workers + (id -> worker), completed)
      case NewTask(uri, replyTo) =>
        replyTo ! id
        withScheduled(
          id + 1,
          workers,
          completed,
          immutable.Queue(TaskState.Scheduled(uri, id)))
      case ListTasks(replyTo) =>
        replyTo ! (workers.keys.toList ++ completed.keys.toList)
        Behaviors.same
      case GetTaskStatus(taskId, replyTo) =>
        completed
          .get(taskId)
          .map(Right(_))
          .orElse(workers.get(taskId).map(Left(_))) match {
          case Some(Right(completed)) =>
            replyTo ! completed
          case Some(Left(worker)) =>
            worker ! Watcher.GetDetail(replyTo)
          case None =>
            replyTo ! TaskState.Unknown
        }
        Behaviors.same
      case Cancel(taskId, replyTo) =>
        workers.get(taskId).foreach(wrk => wrk ! Watcher.Cancel)
        replyTo ! Done
        Behaviors.same
      case ReadFile(taskId, replyTo) =>
        replyTo ! maybeFileName(completed, taskId)
        Behaviors.same
      case ShutDown =>
        workers.values.foreach(ref => ref ! Watcher.Cancel)
        awaitTermination(workers, completed)
      case td: TaskDone =>
        empty(
          id,
          workers - td.taskId,
          completed + (td.taskId -> td.toDoneState))
      case tf: TaskFailed =>
        empty(
          id,
          workers - tf.taskId,
          completed + (tf.taskId -> tf.toFailedState))
      case tc: TaskCanceled =>
        empty(id, workers - tc.taskId, completed + (tc.taskId -> tc.toCanceled))
    }

  def withScheduled(
    id: Int,
    workers: Map[Int, ActorRef[Watcher.Command]],
    completed: Map[Int, TaskState],
    scheduled: immutable.Queue[TaskState.Scheduled]
  ): Behavior[Command] = {
    def onComplete(
      taskId: Int,
      compl: TaskState
    ): Behavior[Command] = {
      val (hd, nextQueue) = scheduled.dequeue
      val nextWorkers     = workers - taskId + (hd.taskId -> spawnWatcher(ctx, hd))
      val nextCompleted   = completed + (taskId           -> compl)
      if (nextQueue.isEmpty)
        empty(id, nextWorkers, nextCompleted)
      else
        withScheduled(id, nextWorkers, nextCompleted, nextQueue)
    }

    Behaviors.receiveMessage {
      case NewTask(uri, replyTo) =>
        replyTo ! id
        withScheduled(
          id + 1,
          workers,
          completed,
          scheduled.enqueue(TaskState.Scheduled(uri, id)))
      case ListTasks(replyTo) =>
        replyTo !
          workers.keys.toList ++
          completed.keys.toList ++ scheduled.map(_.taskId)
        Behaviors.same
      case GetTaskStatus(taskId, replyTo) =>
        completed
          .get(taskId)
          .orElse(scheduled.find(_.taskId == taskId))
          .map(Right(_))
          .orElse(workers.get(taskId).map(Left(_))) match {
          case Some(Left(worker)) =>
            worker ! Watcher.GetDetail(replyTo)
          case Some(Right(state)) =>
            replyTo ! state
          case None =>
            replyTo ! TaskState.Unknown
        }
        Behaviors.same
      case Cancel(taskId, replyTo) =>
        workers.get(taskId).foreach(wrk => wrk ! Watcher.Cancel)
        replyTo ! Done
        Behaviors.same
      case ReadFile(taskId, replyTo) =>
        replyTo ! maybeFileName(completed, taskId)
        Behaviors.same
      case ShutDown =>
        ctx.log.info("ShutDown called on TaskManager")
        workers.values.foreach(_ ! Watcher.Cancel)
        awaitTermination(workers, completed)
      case TaskDone(taskId, uri, output, lines, avgLinesPerSec) =>
        onComplete(
          taskId,
          TaskState.Done(uri, taskId, lines, avgLinesPerSec, output))
      case TaskFailed(taskId, uri) =>
        onComplete(taskId, TaskState.Failed(uri, taskId))
      case TaskCanceled(taskId, uri) =>
        onComplete(taskId, TaskState.Canceled(uri, taskId))
    }
  }

  def awaitTermination(
    workers: Map[Int, ActorRef[Watcher.Command]],
    completed: Map[Int, TaskState]
  ): Behavior[Command] = {
    ctx.log.info("Awaiting termination of {} worker[s]", workers.size)
    if (workers.isEmpty) {
      ctx.log.info("TaskManager stopped")
      Behaviors.stopped
    } else {
      Behaviors.receiveMessagePartial {
        case ListTasks(replyTo) =>
          replyTo ! completed.keys.toList ++ workers.keys.toList
          Behaviors.same
        case GetTaskStatus(taskId, replyTo) =>
          replyTo !
            completed
              .get(taskId)
              .orElse(
                workers.get(taskId).map(_ => TaskState.Canceled("", taskId)))
              .getOrElse(TaskState.Unknown)
          Behaviors.same
        case ReadFile(taskId, replyTo) =>
          replyTo ! maybeFileName(completed, taskId)
          Behaviors.same
        case TaskDone(taskId, uri, output, lines, avgLinesPerSec) =>
          awaitTermination(
            workers - taskId,
            completed +
              (taskId ->
                TaskState.Done(uri, taskId, lines, avgLinesPerSec, output)))
        case TaskFailed(taskId, uri) =>
          awaitTermination(
            workers - taskId,
            completed + (taskId -> TaskState.Failed(uri, taskId)))
        case TaskCanceled(taskId, uri) =>
          awaitTermination(
            workers - taskId,
            completed + (taskId -> TaskState.Canceled(uri, taskId)))
      }
    }
  }

  private def maybeFileName(
    completed: Map[Int, TaskState],
    taskId: Int
  ): Option[String] =
    completed
      .get(taskId)
      .flatMap {
        case TaskState.Done(_, _, _, _, resultUri) =>
          Some(resultUri)
        case _ =>
          None
      }

  private def spawnWatcher(
    ctx: ActorContext[Command],
    scheduled: TaskState.Scheduled
  ): ActorRef[Watcher.Command] =
    ctx.spawn(
      Watcher(scheduled.taskId, scheduled.uri, ctx.self, conf),
      s"watcher-${scheduled.taskId}")

}
