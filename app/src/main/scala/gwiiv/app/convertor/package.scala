package gwiiv.app

import shapeless.tag
import shapeless.tag._

package object convertor {

  implicit class TaskCompletenessOps[TC <: TaskManager.Command](val tc: TC)
      extends AnyVal {

    def toDoneState(
      implicit
      ev: TC =:= TaskManager.TaskDone
    ): TaskState.Done = {
      val td = ev(tc)
      TaskState.Done(td.uri, td.taskId, td.lines, td.avgLinesPerSec, td.output)
    }

    def toFailedState(
      implicit
      ev: TC =:= TaskManager.TaskFailed
    ): TaskState.Failed = {
      val td = ev(tc)
      TaskState.Failed(td.uri, td.taskId)
    }

    def toCanceled(
      implicit
      ev: TC =:= TaskManager.TaskCanceled
    ): TaskState.Canceled = {
      val td = ev(tc)
      TaskState.Canceled(td.uri, td.taskId)
    }

    def toScheduled(
      id: Int
    )(
      implicit
      ev: TC =:= TaskManager.NewTask
    ): TaskState.Scheduled = TaskState.Scheduled(ev(tc).uri, id)

  }
}
