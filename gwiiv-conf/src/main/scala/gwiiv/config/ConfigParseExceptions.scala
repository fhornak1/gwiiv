package gwiiv.config

import cats.data.NonEmptyChain
import com.typesafe.config.ConfigException

import scala.reflect.ClassTag

object ConfigParseExceptions {
  def apply[C : ClassTag](exceptions: NonEmptyChain[ConfigException]): ConfigParseExceptions[C] =
    new ConfigParseExceptions[C](fmtExceptions(exceptions))

  private def fmtExceptions(excs: NonEmptyChain[ConfigException]): String =
    excs.map(fmtExc).toNonEmptyList.toList.mkString("\n")

  private def fmtExc(ex: ConfigException): String = s"[${ex.getClass.getSimpleName}]: ${ex.getMessage}"
}

class ConfigParseExceptions[C](errors: String)(implicit ct: ClassTag[C])
    extends RuntimeException(
      s"Parsing of configuration ${ct.runtimeClass.getName} failed, due to following errors:\n$errors")
    with Serializable
