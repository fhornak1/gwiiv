package gwiiv.config

import cats.data.{NonEmptyChain, Validated}
import com.typesafe.config.{ConfigException, Config => TConfig}

import scala.reflect.ClassTag

trait ConfigParser[T] {
  import ConfigParser._

  val Path: String = ""

  final def varPath(name: String): String =
    if (Path.isEmpty) name else s"$Path.$name"

  def parse(conf: TConfig): ValidatedConf[T]

  final def parseOrThrow(conf: TConfig)(implicit ct: ClassTag[T]): T =
    parse(conf) match {
      case Validated.Valid(a) => a
      case Validated.Invalid(e) => throw ConfigParseExceptions(e)
    }
}

object ConfigParser {
  type ValidatedConf[T] = Validated[NonEmptyChain[ConfigException], T]

  def checked[T](g: => T): ValidatedConf[T] =
    Validated.catchOnly[ConfigException](g).toValidatedNec
}
