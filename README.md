# GWIiv
**scala challenge solution**

This project contains web app, that converts `csv` files into `json` using provided `uri` of csv file.

**Example**
```bash
$ curl -XPOST 127.0.0.1:8080/task -d "https://some.url.with/data.csv"
{"taskId": 12345, "uri":"https://some.url.with/data.csv"}
```

```bash
$ curl -XGET localhost:8080/task
[1, 2, 3, 4, 5, 6, 7, 8, 9]
```

```bash
$ curl -XGET localhost:8080/task/1
{"id": 1, "status": "DONE", "lines": 86235, "avgLinesPerSecond": 130}
```

## Building Project using gradle
```bash
$ ./gradlew build
```

or if you wan to create `fatjar` you can also use task `shadowJar`

```bash
$ ./gradlew shadowJar
```

### Building Dockerfile

```bash
$ docker build -t gwiiv:0.1.0 .  # or podman build
```

## Running a project

Simplest option without build is to type
```bash
$ ./gradlew run
```

You can also build the project and run it via
```bash
$ ./gradlew shadowJar && java -jar ./app/build/libs/app-0.1.0-SNAPSHOT-shaded.jar
```

## Configuration
Every config variable can be configured from 